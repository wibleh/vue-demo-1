
window.Vue = require('vue');

import router from './router';

import App from './components/App';

const app = new Vue({
    router,
    el: '#app',
    template: '<App />',
    components: {
        App
    }
});