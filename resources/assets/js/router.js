import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from './components/Home.vue';
import ColourAveraging from './components/ColourAveraging.vue';
import ArrayToHtml from './components/ArrayToHtml.vue';

Vue.use(VueRouter);

export default new VueRouter({
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home
        },
        {
            path: '/colour-averaging',
            name: 'ColourAveraging',
            component: ColourAveraging
        },
        {
            path: '/array-to-html',
            name: 'ArrayToHtml',
            component: ArrayToHtml
        }
    ]
});
